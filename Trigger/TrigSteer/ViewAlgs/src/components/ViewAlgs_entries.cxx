

#include "../EventViewCreatorAlgorithm.h"
#include "../MergeViews.h"
#include "../ViewCreatorInitialROITool.h"
#include "../ViewCreatorPreviousROITool.h"
#include "../ViewCreatorNamedROITool.h"
#include "../ViewCreatorFSROITool.h"
#include "../ViewCreatorFetchFromViewROITool.h"
#include "../ViewCreatorCentredOnIParticleROITool.h"
#include "../ViewCreatorCentredOnClusterROITool.h"
#include "../ViewCreatorCentredOnJetWithPVConstraintROITool.h"

DECLARE_COMPONENT( EventViewCreatorAlgorithm )
DECLARE_COMPONENT( MergeViews )
DECLARE_COMPONENT( ViewCreatorInitialROITool )
DECLARE_COMPONENT( ViewCreatorPreviousROITool )
DECLARE_COMPONENT( ViewCreatorNamedROITool )
DECLARE_COMPONENT( ViewCreatorFSROITool )
DECLARE_COMPONENT( ViewCreatorFetchFromViewROITool )
DECLARE_COMPONENT( ViewCreatorCentredOnIParticleROITool )
DECLARE_COMPONENT( ViewCreatorCentredOnClusterROITool )
DECLARE_COMPONENT( ViewCreatorCentredOnJetWithPVConstraintROITool )

