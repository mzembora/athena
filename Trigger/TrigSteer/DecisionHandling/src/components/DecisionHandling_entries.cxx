#include "../DumpDecisions.h"
#include "../RoRSeqFilter.h"
#include "../TriggerSummaryAlg.h"
#include "DecisionHandling/ComboHypo.h"
#include "../InputMakerForRoI.h"
#include "../DeltaRRoIComboHypoTool.h"
#include "DecisionHandling/ComboHypoToolBase.h"

DECLARE_COMPONENT( DumpDecisions )
DECLARE_COMPONENT( RoRSeqFilter )
DECLARE_COMPONENT( TriggerSummaryAlg )
DECLARE_COMPONENT( ComboHypo )
DECLARE_COMPONENT( InputMakerForRoI )
DECLARE_COMPONENT( ComboHypoToolBase )
DECLARE_COMPONENT( DeltaRRoIComboHypoTool )


